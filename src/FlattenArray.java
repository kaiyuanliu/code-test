import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class FlattenArray {

    public static void main(String[] args) {
        List<Object> integers = asList(asList(1, 2, asList(3)), 4);

        long startTime = System.nanoTime();
        List<Object> flattenIntegers = flatten(integers);
        long endTime = System.nanoTime();

        long startTime1 = System.nanoTime();
        List<Object> flattenIntegersWithoutRecursion = flattenWithoutRecursion(integers);
        long endTime1 = System.nanoTime();

        System.out.println("needFlattenList: " + integers.toString());
        System.out.println("flattenIntegers with recursion: " + flattenIntegers.toString());
        System.out.println("time consuming: " + (endTime - startTime));

        System.out.println("flattenIntegers without recursion: " + flattenIntegersWithoutRecursion.toString());
        System.out.println("time consuming: " + (endTime1 - startTime1));

    }

    public static List<Object> flatten(List<Object> needFlattenList) {
        List<Object> newList = new LinkedList<>();
        flatten(needFlattenList, newList);
        return newList;
    }

    public static void flatten(List<?> sourceList, List<Object> destList) {
        for (Object val: sourceList) {
            // if item is an instance of list, call flatten again(recursion)
            if (val instanceof List<?>) {
                flatten((List<?>) val, destList);
            } else {
                destList.add(val);
            }
        }
    }

    public static List<Object> flattenWithoutRecursion(List<Object> needFlattenList) {
        List<Object> newList = new ArrayList<>();
        LinkedList<Object> objectLinkedList = new LinkedList<>(needFlattenList);
        while (objectLinkedList.size() > 0) {
            Object removedItem = objectLinkedList.remove();
            if (removedItem instanceof List<?>) {
                objectLinkedList.addAll(0, (List<?>)removedItem);
            } else {
                newList.add(removedItem);
            }
        }

        return newList;
    }

    private static List<Object> asList(Object... args) {
        return Arrays.asList(args);
    }

}
